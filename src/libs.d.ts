declare module 'react-slideshow-image' {
    interface SliderProps {
        children?: any
        duration?: number
        transitionDuration?: number
        infinite?: boolean
        indicators?: boolean
        arrows?: boolean
        autoplay?: boolean
    }

    export class Slide extends React.Component<SliderProps, any> {}
}
